<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\CompanyController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// employee
Route::get('/pegawai', [EmployeeController::class, 'index'])->name('pegawai');

// tambah data
Route::get('/tambahpegawai', [EmployeeController::class, 'tambahpegawai'])->name('tambahpegawai');
Route::post('/insertdata', [EmployeeController::class, 'insertdata'])->name('insertdata');

// update
Route::get('/tampilkandata/{id}', [EmployeeController::class, 'tampilkandata'])->name('tampilkandata');
Route::post('/updatedata/{id}', [EmployeeController::class, 'updatedata'])->name('updatedata');

// delete
Route::get('/deletedata/{id}', [EmployeeController::class, 'deletedata'])->name('deletedata');


// company
Route::get('/perusahaan', [CompanyController::class, 'index'])->name('perusahaan');

// tambah data
Route::get('/tambahperusahaan', [CompanyController::class, 'tambahperusahaan'])->name('tambahperusahaan');
Route::post('/insertdata_perusahaan', [CompanyController::class, 'insertdata_perusahaan'])->name('insertdata_perusahaan');

// update
Route::get('/tampildata_perusahaan/{id}', [CompanyController::class, 'tampildata_perusahaan'])->name('tampildata_perusahaan');
Route::post('/updatedata_perusahaan/{id}', [CompanyController::class, 'updatedata_perusahaan'])->name('updatedata_perusahaan');

// delete
Route::get('/deletedata_perusahaan/{id}', [CompanyController::class, 'deletedata_perusahaan'])->name('deletedata_perusahaan');

// export pdf
Route::get('/exportpdf', [EmployeeController::class, 'exportpdf'])->name('exportpdf');

// export excel
Route::get('/exportExcel', [EmployeeController::class, 'exportExcel'])->name('exportExcel');
