<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CRUD LARAVEL</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
  </head>
  <body>
    <h2 class="text-center my-4">Data Company</h2>
    <div class="container">
      <a href="/tambahperusahaan" class="btn btn-success">Tambah +</a>
      <div class="row">
        <div class="my-2">
          @if ($message = Session::get('success'))
            <div class="alert alert-success" role="alert">
              {{ $message }}
            </div>          
          @endif
        </div>
        <table class="table table-striped text-center">
          <thead>
            <tr>
              <th scope="col">id</th>
              <th scope="col">Nama</th>
              <th scope="col">Alamat</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($data as $row)
            <tr>
              <th scope="row">{{ $row->id }}</th>
              <td>{{ $row->nama }}</td>
              <td>{{ $row->alamat }}</td>
              <td>
                <a href="/tampildata_perusahaan/{{ $row->id }}" class="btn btn-primary">Update</a>
                <a href="/deletedata_perusahaan/{{ $row->id }}" class="btn btn-danger">Delete</a>
              </td>
            </tr>                
            @endforeach
          </tbody>
        </table>
      </div>
    </div>





    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
  </body>
</html>