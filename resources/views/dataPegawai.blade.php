<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CRUD LARAVEL</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.css" integrity="sha512-oe8OpYjBaDWPt2VmSFR+qYOdnTjeV9QPLJUeqZyprDEQvQLJ9C5PCFclxwNuvb/GQgQngdCXzKSFltuHD3eCxA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  </head>
  <body>
    <h2 class="text-center my-4">Data Employee</h2>
    <div class="container">
      <a href="/tambahpegawai" class="btn btn-success">Tambah +</a>

      <div class="row g-3 align-items-center mt-2">
        <div class="col-auto">
          <form action="/pegawai" method="GET">
            <input type="search" name="search" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline">
          </form>
        </div>
        <div class="col-auto">
          <a href="/exportpdf" class="btn btn-info">Export PDF</a>
        </div>
        <div class="col-auto">
          <a href="/exportExcel" class="btn btn-success">Export Excel</a>
        </div>
      </div>


      <div class="row">
        <div class="my-2">
          @if ($message = Session::get('success'))
            <div class="alert alert-success" role="alert">
              {{ $message }}
            </div>          
          @endif
        </div>
        <table class="table table-striped text-center">
          <thead>
            <tr>
              <th scope="col">id</th>
              <th scope="col">Nama</th>
              <th scope="col">Posisi</th>
              <th scope="col">Perusahaan</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($data as $row)
            <tr>
              <th scope="row">{{ $row->id }}</th>
              <td>{{ $row->nama }}</td>
              <td>{{ $row->atasan_id }}</td>
              <td>{{ $row->company_id }}</td>
              <td>
                <a href="/tampilkandata/{{ $row->id }}" class="btn btn-primary">Update</a>
                <a href="#" class="btn btn-danger delete" data-id="{{ $row->id }}" data-nama="{{ $row->nama }}">Delete</a>
              </td>
            </tr>                
            @endforeach
          </tbody>
        </table>
        {{ $data->links() }}
      </div>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

    <script
    src="https://code.jquery.com/jquery-3.6.0.slim.min.js"
    integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI="
    crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js" integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  </body>
  <script>
    $('.delete').click(function(){
      var pegawaiid = $(this).attr('data-id');
      var nama = $(this).attr('data-nama');
        swal({
          title: "Yakin!!",
          text: "Apakah kamu yakin ingin menghapus data " +nama+ " ",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
          window.location = "/delete/{id}"+nama+ " "
          swal("Data berhasil dihapus", {
            icon: "success",
          });
        } else {
          swal("Data tidak jadi terhapus");
        }
      });
    });
  </script>
  
  <script>
    // toastr.success('Have fun storming the castle!', 'Miracle Max Says')
  </script>
</html>