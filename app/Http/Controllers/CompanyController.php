<?php

namespace App\Http\Controllers;

// use App\Models\employee;

use App\Models\company;
use Illuminate\Http\Request;
use PDF;

class CompanyController extends Controller
{
    public function index()
    {
        $data = company::all();
        return view('dataPerusahaan', compact('data'));
    }

    public function tambahperusahaan()
    {
        return view('tambahdata_perusahaan');
    }

    public function insertdata_perusahaan(Request $request)
    {
        // dd($request->all());
        company::create($request->all());
        return redirect()->route('perusahaan')->with('success', 'data berhasil ditambahkan');
    }

    public function tampildata_perusahaan($id)
    {
        $data = company::find($id);
        // dd($data);
        return view('tampildata_perusahaan', compact('data'));
    }

    public function updatedata_perusahaan(Request $request, $id)
    {
        $data = company::find($id);
        $data->update($request->all());
        return redirect()->route('perusahaan')->with('success', 'data berhasil update');
    }

    public function deletedata_perusahaan($id)
    {
        $data = company::find($id);
        $data->delete();
        return redirect()->route('perusahaan')->with('success', 'data berhasil dihapus');
    }

    // public function exportpdf()
    // {
    //     $data = company::all();
    //     view()->share('data', $data);
    //     $pdf = PDF::loadview('dataPegawai-pdf');
    //     return $pdf->download('data.pdf');
    // }
}
