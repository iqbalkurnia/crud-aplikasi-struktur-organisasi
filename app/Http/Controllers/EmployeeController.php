<?php

namespace App\Http\Controllers;

use App\Models\employee;
use Illuminate\Http\Request;
use PDF;
use Rap2hpoutre\FastExcel\FastExcel;
// use App\User;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('search')) {

            $data = employee::where('nama', 'Like', '%' . $request->search . '%')->paginate(9);
        } else {

            $data = employee::paginate(9);
        }
        return view('dataPegawai', compact('data'));
    }

    public function tambahpegawai()
    {
        return view('tambahData');
    }

    public function insertdata(Request $request)
    {
        // dd($request->all());
        employee::create($request->all());
        return redirect()->route('pegawai')->with('success', 'data berhasil ditambahkan');
    }

    public function tampilkandata($id)
    {
        $data = employee::find($id);
        // dd($data);
        return view('tampilData', compact('data'));
    }

    public function updatedata(Request $request, $id)
    {
        $data = employee::find($id);
        $data->update($request->all());
        return redirect()->route('pegawai')->with('success', 'data berhasil update');
    }

    public function deletedata($id)
    {
        $data = employee::find($id);
        $data->delete();
        return redirect()->route('pegawai')->with('success', 'data berhasil dihapus');
    }

    public function exportpdf()
    {
        $data = employee::all();
        view()->share('data', $data);
        $pdf = PDF::loadview('dataPegawai-pdf');
        return $pdf->download('data.pdf');
    }

    public function exportExcel()
    {
        // $data = employee::all();

        return (new FastExcel(employee::all()))->download('data.xlsx', function ($data) {
            return [
                'id' => $data->id,
                'nama' => $data->nama,
                'Posisi' => $data->atasan_id,
                'Perusahaan' => $data->company_id,

            ];
        });
    }
}
